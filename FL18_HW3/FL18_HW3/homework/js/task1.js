// Your code goes here
let startMoney = prompt('Enter amount of money');
validationStartMoney(startMoney);
function validationStartMoney(number) {
  if(isNaN(startMoney) || startMoney < 1000) {
    alert('Invalid input data');
    startMoney = prompt('Enter amount of money');
    validationStartMoney(startMoney);
  } else {
    return number;
  }
}
let year = prompt('Enter number of years');
validYear(year);
function validYear(numberYear) {
  if(Number.isInteger(Number(numberYear)) && numberYear >= 1) {
    return numberYear;
  } else {
    alert('Invalid input data');
    year = prompt('Enter number of years');
    validYear(year);
  }
}
let percent = prompt('Enter percentage of a year');
validPercent(percent)
function validPercent(per) {
  if(isNaN(per) || per > 100) {
    alert('Invalid input data');
    percent = prompt('Enter percentage of a year');
    validPercent(percent);
  }
}
let amountProfit = 0;
let oneYear = 0;
let i = 0;
let finalMoney = startMoney;

function profit() {
    while(year>i) {
      oneYear = (Number(finalMoney) * (percent/100)).toFixed(2);
      amountProfit = (Number(oneYear) + Number(amountProfit)).toFixed(2);
      finalMoney = (Number(finalMoney) + Number(oneYear)).toFixed(2); 
      i++;
    }
    alert(`Initial amount: ${startMoney}
Number of years: ${year}
Percentage of year: ${percent}
  
Total profit: ${amountProfit}
Total amount: ${finalMoney}`);
  }
  profit();